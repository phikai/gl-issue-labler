<?php

require_once 'vendor/autoload.php';
require_once 'inc/auth.php';

$client = \Gitlab\Client::create('https://gitlab.com/')
	->authenticate(GITLAB_TOKEN, \Gitlab\Client::AUTH_URL_TOKEN)
;

$project_id = 12311244;
$search_label = 'testinitial';

$pager = new \Gitlab\ResultPager($client);
$issues = $pager->fetchAll($client->api('issues'),'all',[$project_id, ['state' => 'opened', 'labels' => "$search_label"]]);

print_r($issues);

foreach ($issues as $issue) {
	$issue_iid = $issue['iid'];
	$labels = $issue['labels'];
	array_push($labels, 'testapi');
	$client->api('issues')->update(12311244, $issue_iid, ['labels' => implode(', ', $labels)]);
}